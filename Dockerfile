FROM solr:5.5-alpine
ENV SOLR_CORE_DIR /opt/solr/server/solr/mycores/blacklight

# Create blacklight Solr core directory
RUN mkdir -p $SOLR_CORE_DIR
WORKDIR $SOLR_CORE_DIR

COPY prod/conf ./conf
RUN touch "$SOLR_CORE_DIR/core.properties"
